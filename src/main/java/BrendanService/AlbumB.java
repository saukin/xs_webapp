
package BrendanService;

import javax.inject.Named;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity class for Brendan Wood service
 * 
 * @author saukin
 */
@XmlRootElement
@Named("albumB")
public class AlbumB {

    /**
     *
     */
    protected String upc;
    
    /**
     *
     */
    protected String albumName;
    
    /**
     *
     */
    protected String pressingYear;
    
    /**
     *
     */
    protected String artist;

    /**
     *
     * @return
     */
    public String getUpc() {
        return upc;
    }

    /**
     *
     * @param upc
     */
    public void setUpc(String upc) {
        this.upc = upc;
    }

    /**
     *
     * @return
     */
    public String getAlbumName() {
        return albumName;
    }

    /**
     *
     * @param albumName
     */
    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    /**
     *
     * @return
     */
    public String getPressingYear() {
        return pressingYear;
    }

    /**
     *
     * @param pressingYear
     */
    public void setPressingYear(String pressingYear) {
        this.pressingYear = pressingYear;
    }

    /**
     *
     * @return
     */
    public String getArtist() {
        return artist;
    }

    /**
     *
     * @param artist
     */
    public void setArtist(String artist) {
        this.artist = artist;
    }

    @Override
    public String toString() {
        return "Album{" + "upc=" + upc + ", albumName=" + albumName + ", pressingYear=" + pressingYear + ", artist=" + artist + '}';
    }
    
    

}
