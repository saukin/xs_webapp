
package BrendanService;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import java.util.ArrayList;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import xs.client.services.AlbumClient;
import xs.client.webapp.beans.AlbumBean;
import xs.client.webapp.beans.UserBean;
import xs.client.webapp.controller.ListManager;

/**
 * Client bean for Brendan Wood service
 * 
 * @author saukin
 */
@Named("brendanService")
@RequestScoped
public class BrendanService {
    
    ClientConfig config = new DefaultClientConfig();

    Client client = Client.create(config);
    
    WebResource wr = client.resource(UriBuilder.fromUri("http://localhost:8080/XS_PROJECT/webresources/BrendanService/").build());

   
    @Inject
    AlbumBean albumBean;
    @Inject
    UserBean userBean;
    @Inject
    ListManager listManager;
    @Inject
    AlbumClient albumClient;
    
    private ArrayList<String> upc = new ArrayList<>();

//    public String getUpc() {
//        return upc;
//    }
//


    /**
     *
     * @param upc - ArrayList with available upc values
     */
    
    public void setUpc(ArrayList<String> upc) {
        this.upc = upc;
//        return "editFromUPC";
    }
    
    /**
     *
     * @return ArrayList with available upc values for data table on a jsf page
     */
    public ArrayList<String> getUpc() {
        upc.clear();
        String instructions = wr.accept(MediaType.TEXT_PLAIN).get(String.class);
        int i = instructions.indexOf(':');
        instructions = instructions.substring(i+1, instructions.length()).trim();
        
        
        String[] s = instructions.split(",");
        for (String item : s) {
            upc.add(item.trim());
        }
        
        return upc;
    }
    
    /**
     *
     * @param upc - String value of album upc
     * @return data about selected album in data table on a jsf page
     * @throws Exception
     */
    public String getAlbumInfo(String upc) throws Exception {
        
        FacesContext fc = FacesContext.getCurrentInstance();
        upc = getUpcParam(fc);
        
        AlbumB aB = wr.path(upc).type(MediaType.TEXT_PLAIN).accept(MediaType.APPLICATION_JSON).get(AlbumB.class);
        
        albumBean.setAlbum_name(aB.getAlbumName());
        albumBean.setArtist_group(aB.getArtist());
        albumBean.setUpc_code(upc);
        albumBean.setPressing_year(Integer.parseInt(aB.getPressingYear()));
        listManager.setAlbumList(albumClient.getActiveAlbumsByUserID(String.valueOf(userBean.getUser_id()), true));
        return listManager.getPage();
    }
    
    /**
     *
     * @param fc value of jsf param
     * @return String value of an item fro data table on a jsf page
     */
    public String getUpcParam(FacesContext fc) {

        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        return params.get("upcItem");

    }
    
    
    
}
